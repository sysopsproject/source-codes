#define _REENTRANT    /* basic 3-lines for threads */
#include <pthread.h>
#include <thread.h>
#define NUM_THREADS 5
#define SLEEP_TIME 10

void *PrintHello()
{
   cout << "Hello World!";
}

int main(int argc, char *argv[]) {
    for ( i = 0; i < NUM_THREADS; i++)
         thr_create(NULL, 0, &PrintHello, (void *)SLEEP_TIME, 0,
         &tid[i]);
     while (thr_join(0, NULL, NULL) == 0)
         continue;
     break;
}