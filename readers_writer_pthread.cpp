/* 
    implements the 'classic' solution to the readers writers problem which
    prioritizes readers
    - uses pthreads
    - stack size of the individual threads is set to STACKSIZE (small value), 
      to permit a large number of threads
    - the shared resource is a file RESSFILE
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <pthread.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <fcntl.h>
#include <limits.h>

#define MAXTHREADS 20000
#define STACKSIZE 65536

#define RESSFILE "zahl.dat"
#define ITERATIONS 100
#define INITVALUE 0

/* some helper functions */
void fatal(char*);
void write_file(void);
void read_file(void);

/*
   global resources 
*/
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t wri = PTHREAD_MUTEX_INITIALIZER;
int rc;                /* reader counter */
unsigned long readers, writers;

/*
    a fatal error has occured -> print a message, errno and exit 
*/
void fatal(char *msg)
{
  assert (msg != NULL);

  perror(msg);
  exit(EXIT_FAILURE);
}

/*
    a non-fatal error has occured -> warn, but continue
*/
void nonfatal(char *msg)
{
  assert (msg != NULL);

  perror(msg);
  return;
}

void* writer(void* in)
{
  int c;

  printf("Writer, TID %x, starts\n", pthread_self());

  for (c=0; c < ITERATIONS; c++) {
    pthread_mutex_lock(&mutex);
    write_file();
    pthread_mutex_unlock(&mutex);
  }

  printf("Writer, TID %x, finished\n", pthread_self());
  pthread_exit(NULL);
}

void* reader(void* in)
{
  int c, ret;

  printf("Reader, TID %x, starts\n", pthread_self());

  for (c=0; c < ITERATIONS; c++) {
    pthread_mutex_lock(&mutex);
    rc += 1;

    if (rc == 1) {
      pthread_mutex_lock(&wri);
    } 
    pthread_mutex_unlock(&mutex);

    read_file();
    pthread_mutex_lock(&mutex);
    rc -= 1;

    if (rc == 0) {
      pthread_mutex_unlock(&wri);
    } 
    pthread_mutex_unlock(&mutex);
  }

  printf("Reader, TID %x, finished\n", pthread_self());
  pthread_exit(NULL);
}

/*
    must be called atomically by writers
*/
void write_file(void)
{
  FILE *fin;
  int ret;
  unsigned long x;

  fin = fopen(RESSFILE, "r+");
  if (fin == NULL) {
    fatal("fopen for write");
  }

  ret = fscanf(fin, "%ld", &x); /* returns # of converted items */
  if (ret != 1) {
    fatal("fscanf during write access");
  }

  x+=1;
  rewind(fin);
  ret = fprintf(fin, "%ld\n", x);
  if (ret == -1){
    fatal("fprintf");
  }
  
  ret = fclose(fin);
  if (ret == EOF) {
    fatal("fclose");
  }
}

void read_file(void)
{
  FILE *fin;
  int ret;
  unsigned long x;

  fin = fopen(RESSFILE, "r");
  if (fin == NULL) {
    fatal ("fopen for reading");
  }

  ret = fscanf(fin, "%ld", &x); /* returns # of converted items */
  if (ret != 1) {
    fatal ("fscanf");
  }

  ret = fclose(fin);
  if (ret == EOF) {
    fatal ("fclose after reading");
  }
}


int main(int argc, char* argv[])
{
  pthread_t son[MAXTHREADS];
  pthread_attr_t tattr;
  int c, ret;
  int *retth;
  FILE *fin;
  unsigned long x;

  /* parse command line parameters */
  if (argc != 3) {
    printf("Usage: %s <readers> <writers>\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  readers = strtoul(argv[1], NULL, 10);
  writers = strtoul(argv[2], NULL, 10);
  if ((writers == ULONG_MAX) || (readers == ULONG_MAX)) {
    printf("Could not convert number of readers of writers.\n");
    exit(EXIT_FAILURE);
  }
  if (readers+writers > MAXTHREADS) {
    printf("Maximum Number of threads (%i) exceeded.\n", MAXTHREADS);
    exit(EXIT_FAILURE);
  }
  printf("Creating %d readers and %d writers.\n", readers, writers);

  /* create and reset shared file */
  fin = fopen(RESSFILE, "w");
  if (fin == NULL) {
    fatal("main, fopen for write");
  }
  fprintf(fin, "%ld\n", INITVALUE);
  fclose(fin);

  /* reduce stack size of threads to be created */
  pthread_attr_init(&tattr);
  ret = pthread_attr_setstacksize(&tattr, STACKSIZE);
  if (ret != 0) {
    printf("pthread_attr_setstacksize() failed, error=%d\n", ret);
  }  

  /* create threads, readers first */
  for (c=0; c<readers; c++) {
    ret = pthread_create(&son[c], &tattr, &reader, NULL);
    if (ret != 0) {
      fatal("pthread_create(), reader");
    }
  }

  /* create writer threads */
  for ( ; c<readers+writers; c++) {
    ret = pthread_create(&son[c], &tattr, &writer, NULL);
    if (ret != 0) {
      fatal("pthread_create, writer");
    }
  }
    
  /* only father reaches this */

  /* wait for threads to terminate */
  for (c=0; c<readers+writers; c++) {
    pthread_join(son[c], (void**) NULL);
    printf("Thread no.%d, TID %x, returned %d.\n", c, son[c], ret);
  }

  /* open shared file and check its final value */
  fin = fopen(RESSFILE, "r");
  if (fin == NULL) {
    nonfatal("main, fopen() for check");
  }
  ret = fscanf(fin, "%ld", &x); /* returns # of converted items */
  if (ret != 1) {
    nonfatal ("main, fscanf() while checking");
  }
  fclose(fin);

  if (x == writers*ITERATIONS) {
    printf("Correct value %ld in %s reached.\nDeleting.", x, RESSFILE);
    ret = unlink(RESSFILE);
    if (ret == -1) {
      fatal("unlink");
    }  
  }
  else {
    printf("Incorrect value %ld in %s .\n", x, RESSFILE);
    printf("Leaving file for inspection.\n");
  }
  exit(EXIT_SUCCESS);
}
